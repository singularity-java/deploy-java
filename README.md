# deploy-java

## About

This is an applicatoin to wrap a backend written on Spring Boot using package managers like Maven or Gradle and deploy them using Docker.

## Running

To run the application you just need to type in the one of the folders: either deploy-gradle or deploy-maven
```bash
docker-compose up -d
```

## Screenshots

### deploy-maven

![maven](https://i.imgur.com/wO8sRi0.png)

### deploy-gradle

![gradle](https://i.imgur.com/3jupnuu.png)
